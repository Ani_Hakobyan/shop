#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3  2018

@author: Ani Hakobyan
"""

class Product():
    def __init__(self, id,name,price,quantity):
        self.id = id
        self.name = name
        self.quantity = quantity
        self.price = price
    @classmethod
    def getName(self,id):
        for product in shopProducts:
            if product["id"] == id:
                return product["name"]
    @classmethod
    def getPrice(self,id):
        for product in shopProducts:
            if product["id"] == id:
                return product["price"]
    @classmethod  
    def decProductQuantity(self,id, quantity):
        for product in shopProducts:
            if product["id"] == id:
                if quantity < product["quantity"]:
                    product["quantity"] -= quantity
                    return True
                if quantity == product["quantity"]:
                    Product.removeProduct(id)
                    return True
                print ("There are not {} products".format(quantity))
                return False                        
        
    
    @classmethod  
    def incProductQuantity(self,item):
        for product in shopProducts:
            if product["id"] == item.id:
                product["quantity"] += item.quantity
                return True
        Product.addProduct(item)
        return False
                   
    def addProduct(self):
        shopProducts.append({'id':Product.generateID(shopProducts), 'name':self.name,'price':self.price, 'quantity':self.quantity})
        print ("you successfully added {} into shop".format(self.name))
        Product.updateJsonFile(shop_json,shopProducts)
    
    @classmethod
    def generateID(self,_list):
       for i in range(len(_list)):
            if _list[i]["id"] != i+1:
                return i+1
       return len(_list)+1
      
    @classmethod
    def getProduct(self,id):
        for product in shopProducts:
            if product["id"] == id:
                return "{} {} {}".format(product["name"],product["price"], product["quantity"])
        return False
    
    @classmethod
    def getAllProducts(self):       
        return shopProducts
    
    @classmethod
    def editPrice(self,id,price):   
        for product in shopProducts:
            if product["id"] == id:
                product["price"] = price
                break
        print ("you successfully edited product price")
        Product.updateJsonFile(shop_json, shopProducts)
    
    @classmethod
    def removeProduct(self,id):
        for i in range(len(shopProducts)):
            if shopProducts[i]["id"] == id:
                shopProducts.pop(i)
                break
        print ("you successfully removed product from shop")
        Product.updateJsonFile(shop_json, shopProducts)
     
    @classmethod
    def getJsonPath(self):
        return shop_json
        
    @classmethod  
    def updateJsonFile(self,filename, list_products):        
        os.remove(filename)
        with open(filename, 'w') as f:
            json.dump(list_products, f, indent=4)
           
import json
import os

shop_json = os.path.join(os.path.expanduser('~'), 'PycharmProjects', 'shop','json_files', 'shop')
with open(shop_json, 'r') as f:
    shopProducts = json.load(f)


