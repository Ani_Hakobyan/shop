#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4  2018

@author: Ani Hakobyan
"""
from UserBasket import UserBasket
from Product import Product
from Manager import isTypeCorrect
from Manager import printShopProducts

class UserShoping:
    @classmethod       
    def chooseProc(self):
        while True:
            inData = input("BUY : 1 \nCHANGE  quantity of bought product : 2 \nREMOVE bought product: 3\nEXIT : 4\n: ")
            if isTypeCorrect(inData,int):
                break
        inData = isTypeCorrect(inData,int)
        if inData == 1:
            return buyProduct()
        if inData == 2:
            return changeQuantity()
        if inData == 3:
            return remove()
        if inData == 4:
            print ("bye bye )))" )
            return
        print ("Enter 1, 2, 3 or 4")
        return UserShoping.chooseProc()
    
# input product id and qauntity for buy it          
def buyProduct():
    inData = input("BACK : 1\nCONTINUE : 2\n")
    if not isTypeCorrect(inData,int):
        buyProduct()
    inData = isTypeCorrect(inData,int)
    if inData == 1:
        return UserShoping.chooseProc()
    if inData == 2:
        print ("id  product  price   qauntity" )
        printShopProducts()
        #input product id
        while True:
            id = input("Enter id_ product\n : ")
            if isTypeCorrect(id,int):
                break
        id = isTypeCorrect(id,int)
        #check product with id is exists
        if Product.getProduct(id):
            print (Product.getProduct(id))
            #input quantity
            while True:
                quantity = input("Enter quantity\n : ")
                if isTypeCorrect(quantity,int):
                    quantity = isTypeCorrect(quantity,int)
                    if quantity > 0:
                        break
                    print ("Enter a positive number")
            #add product into user basket    
            name = Product.getName(id)
            price = Product.getPrice(id)
            if not UserBasket.getProduct(id):       
                p = UserBasket(id,name,quantity,price)
                UserBasket.addProduct(p)
            else:
                UserBasket.incQuantity(id,quantity)
            return buyProduct()
        print ("Product with id {} is not exists".format(id))
        return buyProduct()
    print ("Enter 1 or 2")
    return buyProduct()

# input id and new quantity for change product quantity                    
def changeQuantity():
    inData = input("BACK : 1\nCONTINUE : 2\n")
    if not isTypeCorrect(inData,int):
        changeQuantity()
    inData = isTypeCorrect(inData,int)
    if inData == 1:
        return UserShoping.chooseProc()
    if inData == 2:
        print ("id  product  qauntity" )
        printBasketProducts()
        # input product id
        while True:
            id = input("Enter product ID and change quantity\n : ")
            if isTypeCorrect(id,int):
                break
        id = isTypeCorrect(id,int)
        #check product with id is exists
        if not UserBasket.getProduct(id):
            print ("You did not buy it")
            return changeQuantity()
        #input new quantity
        while True:
            quantity = input("Enter product quantity\n : ")
            if isTypeCorrect(quantity,int):
                quantity = isTypeCorrect(quantity,int)
                if quantity > 0:
                    break
                print ("Enter a positive number")
        UserBasket.editProductQuantity(id,quantity)
        return changeQuantity()
    print ("Enter 1 or 2")
    return changeQuantity()

#remove product from user basket    
def remove():
    inData = input("BACK : 1\nCONTINUE : 2\n")
    if not isTypeCorrect(inData,int):
        changeQuantity()
    inData = isTypeCorrect(inData,int)
    if inData == 1:
        return UserShoping.chooseProc()
    if inData == 2:
       print ("id  product  qauntity" )
       printBasketProducts() 
       #input id
       while True:
           id = input("Enter product id and remove product from basket\n : ")
           if isTypeCorrect(id,int):
               break
       id = isTypeCorrect(id,int)
       #check product with id is exists
       if not UserBasket.getProduct(id):
           print ("You did not buy it")
           return remove()
       #confirm the remove operation and remove product
       print (UserBasket.getProduct(id))
       while True:
           inData = input("Do you want to remove that product?\n YES: 1\n No: enter any int number\n: ")
           if isTypeCorrect(inData,int):
               break
       inData = isTypeCorrect(inData,int)
       if inData == 1:
          UserBasket.removeProduct(id)
       return remove()
    print ("Enter 1 or 2")
    return changeQuantity()

def printBasketProducts():
    products = UserBasket.getAllProducts()
    for product in products:
            print ("{} {} {}".format(product["id"],product["name"],product["quantity"]))


