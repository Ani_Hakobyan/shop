#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3  2018

@author: Ani Hakobyan
"""

from Person import Person
class Worker(Person):    
    def __init__(self,id,fname,lname, salary):  
        self.salary = salary
        Person.id = id
        Person.fname = fname
        Person.lname = lname
                   
    def addWorker(self):
        workers.append({'id':Worker.generateID(workers), 'fname':self.fname, 'lname':self.lname, 'salary':int(self.salary)})
        print ("you successfully added worker")
        updateJsonFile()
    
    @classmethod
    def generateID(self,_list):
       for i in range(len(_list)):
            if _list[i]["id"] != i+1:
                return i + 1
       return len(_list)+1
   
    @classmethod
    def getWorker(self,id):
        for worker in workers:
            if worker["id"] == id:
                return "{} {} {}".format(worker["fname"],worker["lname"],worker["salary"])
        return False
        
    @classmethod
    def getAllWorkers(self):        
        return workers
    
    @classmethod
    def editWorkerSalary(self,id,salary):   
        for worker in workers:
            if worker["id"] == id:
                worker["salary"] = salary
                break;
        print ("you successfully edited worker salary")
        updateJsonFile()
    
    @classmethod
    def removeWorker(self,id):
        for i in range(len(workers)):
            if workers[i]["id"] == id:
                workers.pop(i)
                break
        print ("you successfully removed worker")
        updateJsonFile()
            
import json
import os
worker_json = os.path.join(os.path.expanduser('~'), 'PycharmProjects', 'shop','json_files','worker')

with open(worker_json, 'r') as f:
    workers = json.load(f)

def updateJsonFile():        
    os.remove(worker_json)
    with open(worker_json, 'w') as f:
        json.dump(workers, f, indent=4)
