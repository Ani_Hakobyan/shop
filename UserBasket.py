#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 2018

@author: Ani Hakobyan
"""

from Product import Product

class UserBasket(Product):
    def __init__(self,id,name,quantity,price):
        Product.id = id
        Product.name = name
        Product.quantity = quantity
        Product.price = price
    
    #add product into user basket and decrement it quantity into shop  
    def addProduct(self):
        if Product.decProductQuantity(self.id, self.quantity):
            basketProducts.append({'id':self.id, 'name':self.name, 'quantity':self.quantity, 'price':Product.price})
        #Product.decProductQuantity(self.id,self.quantity)
            print ("you successfully add {} to your basket".format(self.name))
        Product.updateJsonFile(basket_json, basketProducts)
        Product.updateJsonFile(Product.getJsonPath(),Product.getAllProducts())
  
    #increment product quantity into user basket and decrement it into shop    
    @classmethod  
    def incQuantity(self,id, quantity):
        for product in basketProducts:
            if product["id"] == id:
                if Product.decProductQuantity(product["id"],quantity):
                    product["quantity"] += quantity
                break        
        Product.updateJsonFile(basket_json, basketProducts)
        Product.updateJsonFile(Product.getJsonPath(),Product.getAllProducts())
    @classmethod
    def getProduct(self,id):
        for product in basketProducts:
            if product["id"] == id:
                return "{} {} {}".format(product["id"],product["name"],product["quantity"])
        return False
    
    @classmethod
    def getAllProducts(self):
        return basketProducts
 
    @classmethod
    def editProductQuantity(self,id,quantity):
        for product in basketProducts:
            if product["id"] == id:
                product["quantity"] = quantity
                break
        print ("you successfully edited product quantity")
        Product.updateJsonFile(basket_json, basketProducts)
    
    @classmethod
    def removeProduct(self,id):
        for i in range(len(basketProducts)):
            if basketProducts[i]["id"] == id:
                product = Product(id,basketProducts[i]["name"],basketProducts[i]["price"],basketProducts[i]["quantity"])
                Product.incProductQuantity(product)
                basketProducts.pop(i)
                break
        print ("you successfully removed product from basket")
        Product.updateJsonFile(basket_json, basketProducts)
        Product.updateJsonFile(Product.getJsonPath(),Product.getAllProducts())
    
import json
import os
basket_json = os.path.join(os.path.expanduser('~'),'PycharmProjects','shop','json_files','basket')
with open(basket_json, 'r') as f:
    basketProducts = json.load(f)

