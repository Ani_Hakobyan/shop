#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 2018

@author: Ani Hakobyan
"""

class Admin():
    def __init__(self, id, username,password):
        self.id = id
        self.username = username
        self.password = password
    
    @classmethod
    def checkAdmin(self,username,password):
        for admin in admins:
            if admin['username'] == username and admin['password'] == password:
                return True
        return False        
            
import json
import os
admin_json = os.path.join(os.path.expanduser('~'), 'PycharmProjects', 'shop','json_files', 'admin')
with open(admin_json, 'r') as f:
    admins = json.load(f)

def updateJsonFile():        
    os.remove(admin_json)
    with open(admin_json, 'w') as f:
        json.dump(admins, f, indent=4)
     
